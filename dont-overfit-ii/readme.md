### Don't over fit challenge II

## Overview
Given a csv of of randomly generated values for 20,000 samples, generate class
predictions for 19,750 from a labeled set of 250. 

# Attempts
- Random Forest
    - Poor performance at initial testing.
    - Slightly greater than an AUC of .5
- Ensemble learning
    - AUC ~.7, large improvement on performance, but individual components of
      the model seemed to out perform the ensemble(???).
- GLM 
    - So far the best performance (AUC 0.851).
    - Some feature engineering and dimensionality reduction.
    - Probably a good result because I spent the most time on data prep. 
- R/pystan Bayesian inference.
    - TBC
